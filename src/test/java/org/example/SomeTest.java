package org.example;

import com.oracle.truffle.api.RootCallTarget;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Value;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class SomeTest {


    @Test
    public void test_mylang() {
        var code = """
                1+3 +9.3+25
                                
                """;
        try (Context context = Context.create()) {
            Value value = context.eval("myl", "1+1");
            System.out.println(value);
        }

    }
//    @Test
//    public void test_java_on_truffle() {
//        var code = """
//                1+3 +9.3+25
//
//                """;
//        try (Context context = Context.newBuilder().allowAllAccess(true).build()) {
//            Value value = context.eval("java", "1+1");
//            System.out.println(value);
//        }
////        assertEquals((Double) call, 1 + 3 + 9.3 + 25, 0.0001);
//    }
    @Test
    public void test_parser_bigint() {
        var code = """
                1+3 +9.3+9999999999
                       +1.3         
                """;
        MyNode myNode = LangParser.parser(code);
        MyRootNode myRootNode = new MyRootNode(myNode);
        RootCallTarget callTarget = myRootNode.getCallTarget();
        Object call = callTarget.call();
        assertEquals((Double) call, 1 + 3 + 9.3 + 9999999999L+1.3, 0.0001);
    }
    @Test
    public void test_parser() {
        var code = """
                1+3 +9.3+25
                       +1.3         
                """;
        MyNode myNode = LangParser.parser(code);
        MyRootNode myRootNode = new MyRootNode(myNode);
        RootCallTarget callTarget = myRootNode.getCallTarget();
        Object call = callTarget.call();
        assertEquals((Double) call, 1 + 3 + 9.3 + 25+1.3, 0.0001);
    }

    @Test
    public void test_it() {
        AddNode addNode = AddNodeGen.create(
                new IntNode(88), new IntNode(99)
        );
        MyRootNode myRootNode = new MyRootNode(addNode);
        RootCallTarget callTarget = myRootNode.getCallTarget();
        var result = callTarget.call();
        assertEquals(88 + 99, result);
    }

    @Test
    public void test_int_overflow() {
        AddNode addNode = AddNodeGen.create(
                new IntNode(Integer.MAX_VALUE), new IntNode(1)
        );
        MyRootNode myRootNode = new MyRootNode(addNode);
        RootCallTarget callTarget = myRootNode.getCallTarget();
        var result = callTarget.call();
        assertEquals(Integer.MAX_VALUE + 1d, result);

    }

    @Test
    public void add_int_double() {
        AddNode addNode = AddNodeGen.create(
                new IntNode(Integer.MAX_VALUE), new DoubleNode(1.1)
        );
        MyRootNode myRootNode = new MyRootNode(addNode);
        RootCallTarget callTarget = myRootNode.getCallTarget();
        var result = callTarget.call();
        assertEquals(Integer.MAX_VALUE + 1.1d, result);

    }
}