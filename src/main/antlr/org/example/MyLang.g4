grammar MyLang;

@header{
    package org.example;
}


prog: expr EOF;

expr: expr '+' expr # addExpr
    | number        # LiteraExpr
    ;

number : INT|DOUBLE;

INT: [0-9]+;
DOUBLE: [0-9]+'.'[0-9]+;

SPACE: (' '| '\n' )+ -> skip;
