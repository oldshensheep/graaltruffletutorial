package org.example;

import com.oracle.truffle.api.CallTarget;
import com.oracle.truffle.api.TruffleLanguage;

@TruffleLanguage.Registration(id = "myl",name = "MyLang")
public final class MyTruffleLang extends TruffleLanguage<Void> {

    @Override
    protected CallTarget parse(ParsingRequest request) throws Exception {
        MyNode myNode = LangParser.parser(request.getSource().getReader());
        MyRootNode rootNode = new MyRootNode(myNode);
        return rootNode.getCallTarget();
    }

    @Override
    protected Void createContext(Env env) {
        return null;
    }
}
