package org.example;

import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.RootNode;

public final class MyRootNode extends RootNode {
    @Child
    MyNode node;

    public MyRootNode(MyNode node) {
        super(null);
        this.node = node;
    }

    @Override
    public Object execute(VirtualFrame frame) {
        return this.node.executeGeneric(frame);
    }
}
