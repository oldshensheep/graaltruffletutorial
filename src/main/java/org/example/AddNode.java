package org.example;

import com.oracle.truffle.api.dsl.NodeChild;
import com.oracle.truffle.api.dsl.Specialization;

@NodeChild("leftNode")
@NodeChild("rightNode")
public abstract class AddNode extends MyNode {


    @Specialization(rewriteOn = ArithmeticException.class)
    protected int doInt(int left, int right) {
        return Math.addExact(left, right);
    }

    // Second, we indicated that the double specialization is a superset of the int one with
    // the replaces attribute; otherwise, both of them could be active at the same time,
    // which would result in generating sub-optimal machine code.
    @Specialization(replaces = "doInt")
    protected double doDouble(double left, double right) {
        return left + right;
    }
}
