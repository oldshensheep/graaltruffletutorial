package org.example;

import org.antlr.v4.runtime.BailErrorStrategy;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.io.IOException;
import java.io.Reader;


public class LangParser {

    public static MyNode parser(String code) {
        return parser(CharStreams.fromString(code));
    }

    public static MyNode parser(Reader reader) throws IOException {
        return parser(CharStreams.fromReader(reader));
    }

    private static MyNode parser(CharStream charStream) {
        var lexer = new MyLangLexer(charStream);
        lexer.removeErrorListeners();
        var parser = new MyLangParser(new CommonTokenStream(lexer));
        parser.removeErrorListeners();

        parser.setErrorHandler(new BailErrorStrategy());
        MyLangParser.ExprContext expr = parser.prog().expr();
        return expr2TruffleNode(expr);
    }

    private static MyNode expr2TruffleNode(MyLangParser.ExprContext expr) {
        if (expr instanceof MyLangParser.AddExprContext) {
            return addExpr2AddNode((MyLangParser.AddExprContext) expr);
        } else {
            return numberExp2LiteraNode((MyLangParser.LiteraExprContext) expr);
        }
    }

    private static MyNode numberExp2LiteraNode(MyLangParser.LiteraExprContext expr) {
        TerminalNode anInt = expr.number().INT();
        if (anInt == null) {
            return parseDoubleLiteral(expr.getText());
        } else {
            return parseIntLiteral(expr.getText());
        }
    }

    private static MyNode parseIntLiteral(String text) {
        try {
            return new IntNode(Integer.parseInt(text));
        } catch (NumberFormatException e) {
            return parseDoubleLiteral(text);
        }
    }

    private static DoubleNode parseDoubleLiteral(String text) {
        return new DoubleNode(Double.parseDouble(text));
    }

    private static AddNode addExpr2AddNode(MyLangParser.AddExprContext expr) {
        return AddNodeGen.create(
                expr2TruffleNode(expr.expr(0)),
                expr2TruffleNode(expr.expr(1))
        );
    }
}
