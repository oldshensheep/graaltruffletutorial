package org.example;

import com.oracle.truffle.api.dsl.ImplicitCast;
import com.oracle.truffle.api.dsl.TypeCast;
import com.oracle.truffle.api.dsl.TypeCheck;
import com.oracle.truffle.api.dsl.TypeSystem;

@TypeSystem()
public abstract class MyTypeSystem {
//    @TypeCheck(double.class)
//    public static boolean isDouble(Object value) {
//        return value instanceof Double || value instanceof Integer;
//
//    }
//
//    @TypeCast(double.class)
//    public static double castDouble(Object value) {
//        if (value instanceof Integer) {
//            return ((Integer) value);
//        } else {
//            return (double) value;
//        }
//    }

    @ImplicitCast
    public static double castIntToDouble(int value) {
        return value;
    }
}
