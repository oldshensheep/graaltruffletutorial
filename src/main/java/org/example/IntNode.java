package org.example;

import com.oracle.truffle.api.frame.VirtualFrame;

public class IntNode extends MyNode {
    private final int value;

    public IntNode(int value) {
        this.value = value;
    }

    @Override
    public int executeInt(VirtualFrame frame) {
        return this.value;
    }

    @Override
    public double executeDouble(VirtualFrame frame) {
        return this.value;
    }

//    @Override
//    public long executeLong(VirtualFrame frame) {
//        return this.value;
//    }

    @Override
    public Object executeGeneric(VirtualFrame frame) {
        return this.value;
    }
}
