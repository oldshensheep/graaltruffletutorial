plugins {
    id("java")
//    id("me.champeau.jmh") version "0.7.1"
    antlr
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(21))
    }
}

val graal_version = "23.1.0"

dependencies {
    implementation("org.graalvm.truffle:truffle-api:$graal_version")
    annotationProcessor("org.graalvm.truffle:truffle-dsl-processor:$graal_version")

    antlr("org.antlr:antlr4:4.12.0")
    testImplementation("org.junit.jupiter:junit-jupiter:5.9.2")
}


tasks.withType<JavaCompile> {
    options.compilerArgs.add("--enable-preview")
}
tasks.test {
    useJUnitPlatform()
    jvmArgs("--enable-preview")
}
